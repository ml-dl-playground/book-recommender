
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

%matplotlib inline

# import books datasets

# ratings.dat
book_ratings_df = pd.read_csv("data/ratings.dat", header=None)

users_df = pd.read_csv("data/users.csv",  sep=";", header=None, encoding='latin-1')
books_df = pd.read_csv("data/books.csv",  sep=";", header=None, encoding='latin-1')

users_df.columns = ["User-ID", "Location", "Age"]

book_ratings_df.columns = ["user_id", "movie_id", "rating", "timestamp"]
book_ratings_df.head()

movie_ratings_df.iloc[0]['movie_id']

# Generate the new dataset

new_books = books_df.iloc[0:3706,:]
new_books['new_id'] = movie_ratings_df['movie_id'].unique()

new_users = users_df.iloc[0:6040,:]
new_users['new_id'] = movie_ratings_df['user_id'].unique()
new_books.head()

# Finding user similarities

from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import correlation, cosine

book_ratings_df  = book_ratings_df.pivot( index='user_id', columns='movie_id', values = "rating" ).reset_index(drop=True)
book_ratings_df.head()

# Fill in missing data with 0
book_ratings_df = user_movies_df.fillna(0)
book_ratings_df.head()

# Calculate the distances
user_similarities = 1 - pairwise_distances(book_ratings_df.as_matrix(), metric="cosine")

user_similarities_df = pd.DataFrame(user_similarities)

user_similarities_df.idxmax(axis=1)[0:5]

# Setting personal correlation to zero
np.fill_diagonal(user_similarities, 0)

user_similarities_df = pd.DataFrame(user_similarities)

user_similarities_df.head()

def get_similar_users(user_id, amount):
    users_df['similarity'] = user_similarities_df.iloc[user_id-1]
    similar = users_df.sort_values(['similarity'], ascending = False)[0:amount]

    return similar

result = get_similar_users(1, 10)
result

original_ratings = pd.read_csv("data/ratings.dat", header=None)
original_ratings.columns = ["user_id", "movie_id", "rating", "timestamp"]

def get_users_top_rated_movies(user_id):
    all_rated_books = original_ratings[original_ratings['user_id'] == user_id]
    sorted_rated_books = all_rated_books.sort_values(['rating'], ascending = False)
    return sorted_rated_books

get_users_top_rated_movies(1).head()

def get_recommended_movies(user_id):
    frames = []
    similar_users = get_similar_users(user_id, 10)
    for index, row in similar_users.iterrows():
        similar_user_id = row['User-ID']
        frames.append(get_users_top_rated_movies(int(similar_user_id)))
    
    return pd.concat(frames).sort_values(['rating'], ascending = False)['movie_id'].values

get_recommended_movies(1)

def filter_books_i_have_not_watched(user_id):
    filtered_movies_id_list = []
    movies_id_list = get_recommended_movies(user_id)
    
    for i in movies_id_list:
        if(book_ratings_df.loc[user_id, i] == 0.0):
            filtered_movies_id_list.append(i)
            
    return filtered_movies_id_list[0:10]

filter_books_i_have_not_watched(1)
